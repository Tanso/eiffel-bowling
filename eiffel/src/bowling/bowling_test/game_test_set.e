note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit
	EQA_TEST_SET

feature -- Test routines

	roll_many(g:GAME; count, points:INTEGER)
		local
			i:INTEGER
		do
			from
				i:=0
			until
				i=count
			loop
				g.roll(points)
				i:=i+1
			end
		end

	roll_spare(g:GAME)
		do
			roll_many(g, 2, 5)
		end

	roll_strike(g:GAME)
		do
			g.roll (10)
		end

	test_gutter_game
		local
			g:GAME
		do
			create g.make()
			roll_many(g,20,0)
			assert("test_gutter_game",g.score() = 0)
		end

	test_all_ones
		local
			g:GAME
		do
			create g.make()
			roll_many(g,20,1)
			assert("test_all_ones",g.score() = 20)
		end

	test_one_spare
		local
			g:GAME
		do
			create g.make()
			roll_spare(g)
			g.roll (3)
			roll_many(g,17,0)
			assert("test_one_spare",g.score() = 16)
		end

	test_one_strike
		local
			g:GAME
		do
			create g.make()
			roll_strike(g)
			g.roll (3)
			g.roll (4)
			roll_many(g,16,0)
			assert("test_one_strike",g.score() = 24)
		end

	test_perfect_game
		local
			g:GAME
		do
			create g.make()
			roll_many(g,12,10)
			assert("test_perfect_game",g.score() = 300)
		end

	test_last_spare
		local
			g:GAME
		do
			create g.make()
			roll_many(g,9,10)
			roll_spare(g)
			g.roll(10)
			assert("test_last_spare",g.score() = 275)
		end

	test_tanso
		local
			g:GAME
		do
			create g.make()
			g.roll(7)
			g.roll(7)
			assert("score_should_not_be_14",not (g.score() = 14))
		end

end
