note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature
	roll_table: ARRAY[INTEGER]
	index: INTEGER

	make
		do
			create roll_table.make_filled (0, 0, 21)
			index := roll_table.lower
		end

	max_ten_pins(): BOOLEAN
		local
			i: INTEGER
		do
			Result := True
			from  i:=0
			until i > 8
			loop
				if(roll_table.item(i*2)+roll_table.item(i*2+1) > 10) then Result := False
				end
				i:=i+1
			end
			if(roll_table.item(18)=10 and roll_table.item(19)<10) then
				if(roll_table.item(19)+roll_table.item(20) > 10) then Result := False
				end
			end
			if(roll_table.item(18)<10 and roll_table.item(18)+roll_table.item(19) > 10) then Result := False
			end
		end

	score(): INTEGER
		local
			sum:INTEGER
			i:INTEGER
		do
			sum:=0

			--PRIMI 9 FRAME
			from
				i:=0
			invariant
				roll_table.valid_index(i)
			until
				i>17
			loop
				--STRIKE
				if (roll_table.item (i) = 10)
				then
					--SE E' STRIKE ANCHE IL SECONDO TIRO SOMMO ANCHE i+4
					if(roll_table.item(i+2)=10)
					then
						sum:=sum+10+roll_table.item(i+2)+roll_table.item(i+4)
					else
						sum:=sum+10+roll_table.item(i+2)+roll_table.item(i+3)
					end
				else
					--SPARE
					if (roll_table.item(i)+roll_table.item(i+1)) = 10
					then
						sum:= sum+10+roll_table.item(i+2)
					else
						sum:= sum + roll_table.item(i)
						sum:= sum + roll_table.item(i+1)
					end
				end
				i:=i+2
			end


			--ULTIMO FRAME

			--STRIKE COL PRIMO TIRO
			if (roll_table.item (i) = 10)
				then
					--DUE TIRI AGGIUNTIVI
					sum:=sum+10+roll_table.item(i+1)+roll_table.item(i+2)
				else
					--SPARE COL PRIMO TIRO
					if (roll_table.item(i)+roll_table.item(i+1)) = 10
					then
						sum:= sum+10+roll_table.item(i+2)
					else
						sum:= sum + roll_table.item(i)
						sum:= sum + roll_table.item(i+1)
					end
				end
			Result:=sum
			ensure
				non_negative_score: Result >= 0
				limited_score:		Result <= 300
		end

	roll(points:INTEGER)
		require
			game_not_over: index < 20 or (index = 20 and (roll_table.item(18)=10 or roll_table.item(18)+roll_table.item(19)=10))
			valid_pins:    points >= 0 and points <= 10
		do
			roll_table.enter (points, index)
			if(points=10 and index < 18 and index\\2 = 0) --STRIKE NEI PRIMI 9 FRAME
			then
				index:=index+2
			else
				index:=index+1
			end
		ensure
			correctness: roll_table.item(old index) = points
			progress: index > (old index)
			non_skip: index < (old index+3)
			score_doesnt_decrease: score >= (old score)
		end

	invariant
		valid_index:            index >= 0 and index <= 21
		non_negative_score:     score >= 0 and score <= 300
		max_ten_pins_per_frame: max_ten_pins
end
